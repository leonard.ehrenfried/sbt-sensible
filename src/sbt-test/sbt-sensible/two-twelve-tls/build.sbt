scalaOrganization in ThisBuild := "org.typelevel"
scalaVersion in ThisBuild := "2.12.3-bin-typelevel-4"

sonatypeGithost := ("gitlab.com", "fommil", "sbt-sensible")
licenses := Seq(Apache2)
