scalaVersion in ThisBuild := "2.12.3"

sonatypeGithost := ("gitlab.com", "fommil", "sbt-sensible")
licenses := Seq(Apache2)
